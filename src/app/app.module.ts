import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';


import { AppComponent } from './app.component';
import { DashboardService } from './services/dashboard.service';
import { LegendComponent } from './components/legend/legend.component';
import { ServicesComponent } from './components/services/services.component';
import { RevenueComponent } from './components/revenue/revenue.component';

@NgModule({
  declarations: [
    AppComponent,
    LegendComponent,
    ServicesComponent,
    RevenueComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [DashboardService, {provide: LOCALE_ID, useValue: 'pt-BR'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
