import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Chart } from 'chart.js';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  pieChart: any;
  data: any;


  constructor(
    private _dashboard: DashboardService,
    private elementRef: ElementRef
  ) {

  }

  ngOnInit() {
    const htmlRef = this.elementRef.nativeElement.querySelector(`#canvas`);

    this.data = this._dashboard.getServices();
    this.pieChart = new Chart(htmlRef, {
      type: 'doughnut',
      data: {
        datasets: [
          {
            data: this.data.values,
            backgroundColor: this.data.colors
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: this.data.keys
      },
      options: {
        cutoutPercentage: 70,
        legend: {
          position: 'bottom center'
        }
      }
    });
  }

}
