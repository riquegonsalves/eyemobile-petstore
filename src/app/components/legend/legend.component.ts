import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-legend",
  templateUrl: "./legend.component.html",
  styleUrls: ["./legend.component.scss"]
})
export class LegendComponent implements OnInit {
  @Input() keys: any;

  @Input() values: any;

  @Input() colors: any;

  @Input() showTotal: Boolean = false;

  total: any = false;

  constructor() {}

  ngOnInit() {
    if (this.showTotal) {
      this.total = this.values.reduce((a, b) => a + b, 0);
    }
  }
}
