import { Component, OnInit, ElementRef } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {

  barChart: any;
  data: any;

  constructor(private _dashboard: DashboardService,
    private elementRef: ElementRef) { }

  ngOnInit() {
    this.data = this._dashboard.getRevenues();

    const barRef = this.elementRef.nativeElement.querySelector(`#barChart`);
    this.barChart = new Chart(barRef, {
      type: "bar",
      options: {
        scales: {
          xAxes: [
            {
              barPercentage: 0.4
            }
          ]
        },
        legend: {
          display: false
        }
      },
      data: {
        datasets: [
          {
            scaleOverride: true,
            scaleSteps: 9,
            scaleStartValue: 0,
            data: this.data.values,
            backgroundColor: this.data.colors
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: this.data.keys
      }
    });
  }

}
