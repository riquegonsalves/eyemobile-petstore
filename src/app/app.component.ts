import { Component, OnInit, ElementRef } from "@angular/core";
import { DashboardService } from "./services/dashboard.service";
import { Chart } from "chart.js";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  total: any;
  pieChart: any;
  barChart: any;
  menu: Boolean;

  constructor(
    private _dashboard: DashboardService,
    private elementRef: ElementRef
  ) {
    this.menu = false;
  }

  ngOnInit() {
    this.total = this._dashboard.getTotal();




  }

  openMenu(enabled: Boolean) {
    if (enabled) {
      return "sidebar d-none d-md-block col-md-2 open";
    } else {
      return "sidebar d-none d-md-block col-md-2";
    }
  }

  toggleMenu() {}
}
