import { Injectable } from '@angular/core';

@Injectable()
export class DashboardService {
  constructor() {}

  getTotal() {
    return {
      value: '10.178,60'
    };
  }

  getServices() {
    return {
      keys: ['BANHO & TOSA', 'CONSULTAS', 'MEDICAMENTOS'],
      values: [6445.27, 3867.15, 2578.1],
      colors: [
        'rgba(205, 41, 213, 1)',
        'rgba(122, 43, 214, 1)',
        'rgba(58, 134, 254, 1)'
      ]
    };
  }

  getRevenues() {
    return {
      keys: ['RECEITAS', 'DESPESAS'],
      values: [12890.5, 2711.9],
      colors: ['rgba(0, 221, 170, 1)', 'rgba(255, 79, 100, 1)']
    };
  }
}
